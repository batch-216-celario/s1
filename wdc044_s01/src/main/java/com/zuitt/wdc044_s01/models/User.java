package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    private long id;
    @Column
    private String username;
    @Column
    private String password;

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Long getId(){
        return id;
    }

    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }


    public void setUsername(){
        this.username = username;
    }
    public void setPassword(){
        this.password = password;
    }
}

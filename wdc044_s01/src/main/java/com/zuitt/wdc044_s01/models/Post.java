package com.zuitt.wdc044_s01.models;

import javax.persistence.*;


import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
//designate table name
@Table(name = "posts")
    public class Post {
        //Indicate that this property represents the primary key
        @Id
        //values for this property will be auto incremented
        @GeneratedValue
        private Long id;
        //class properties that represent table columns in a relational database are annotated as @Column
        @Column
        private String title;
        @Column
        private String content;

        //Empty constructor
        public Post(){}

        //parameterized constructor
        public Post(String title, String content){
            this.title = title;
            this.content = content;
        }
        //Getters and Setters

        public String getTitle(){
            return title;
        }

        public String getContent(){
            return content;
        }

        public void setTitle(){
            this.title=title;
        }
        public void setContent(){
            this.content=content;
        }
    }
